# CBNG Web Service

*Python web service providing interface for [CBNG] java dependency resolver.*

[CBNG]: https://wikis.cern.ch/display/DVTLS/CBNG

## Usage

CBNG Web Service is a python web service based on `gunicorn` that can be easily started by running 
`__main__.py`. The service can be configured with env variables defined in `conf.py`.

The service accepts `product.xml` with java dependencies to resolve as an input. 
The result is a list of resolved dependencies in `JSON` format.

```
[{ 'artifactId': ...,
    'groupId': ...,
    'version': ...,
    'uri': ...,
    'jar_filename': ...
}, 
...
]
```  