import dataclasses
from typing import Generator, List


ARTIFACTORY_URL = r"http://artifactory.cern.ch/development"


@dataclasses.dataclass
class Dependency:
    """
    Base representation of a dependency.

    """

    groupId: str
    artifactId: str
    version: str

    @property
    def uri(self) -> str:
        return f"{ARTIFACTORY_URL}/{self.groupId.replace('.', '/')}/{self.artifactId}/{self.version}/{self.artifactId}-{self.version}.jar"

    @property
    def jar_filename(self) -> str:
        return f"{self.artifactId}-{self.version}.jar"

    def to_dict(self) -> dict:
        return dict(
            groupId=self.groupId,
            artifactId=self.artifactId,
            version=self.version,
            uri=self.uri,
            jar_filename=self.jar_filename
        )

    @classmethod
    def new_from_cbng_line(cls, line: str) -> "Dependency":
        """
        Transform line of format <groupId>:<artifactId>:<version> into a Dependency

        """

        # drop part with lacking groupId in case like "<null>:japc:<null> -> cern.japc:japc:7.8.4"
        if line.strip().startswith("<null>"):
            line = line.split("->")[1]

        line = line.replace('(*)', '').strip()
        if len(line.split('->')) > 1:
            # get the most current version is case like "cern.accsoft.commons:accsoft-commons-util:4.3.2 -> 4.3.4 (*)"
            dependency, version = line.split('->')
            group_id, artifact_id, _ = dependency.strip().split(':')
            version = version.strip()
        else:
            group_id, artifact_id, version = line.strip().split(':')

        return Dependency(groupId=group_id, artifactId=artifact_id, version=version)


class CbngLogParser:

    @classmethod
    def parse_to_flat_dependencies(cls, cbng_logs) -> List[Dependency]:
        """
        Parse CBNG output to a dependency list

        """

        return cls._cbng_tree_to_flat_dependencies(cls._extract_dependencies(cbng_logs))

    @classmethod
    def _cbng_tree_to_flat_dependencies(cls, dependencies_iter: Generator) -> List[Dependency]:
        _included = set()
        result = []
        for line in dependencies_iter:
            dep = Dependency.new_from_cbng_line(line.strip())
            if dep.artifactId not in _included:
                result.append(dep)
                _included.add(dep.artifactId)
        return result

    @classmethod
    def _extract_dependencies(cls, cbng_output: str):
        """
        Cut from CBNG output only lines with dependency tree

        """
        # Tree marks rendered by CBNG to replace
        tree_marks = ['+--- ', '|    ', '\\--- ', '     ']
        to_copy = False
        finish = False

        for line in cbng_output.split("\n"):
            if line.strip().startswith("compile -"):
                to_copy = True
                continue

            if to_copy:
                if line:
                    if line.startswith("[INFO]"):
                        continue
                    if line.startswith('\\---') or not line:
                        finish = False

                    for mark in tree_marks:
                        line = line.replace(mark, '\t')

                    yield line

                    if finish:
                        break
                else:
                    break
