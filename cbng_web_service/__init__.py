from flask import Flask, jsonify, request

from cbng_web_service.cbng_py import cbng_get_resolve_logs
from cbng_web_service.dependencies import CbngLogParser


def create_app():
    app = Flask(__name__)

    @app.route('/resolve/productxml', methods=('POST', 'GET'))
    def resolve():
        if request.method == 'POST':
            # Decode received XML
            product_xml = request.data.decode('utf-8')

            # Run CBNG to resolve dependencies and capture output
            cbng_log = cbng_get_resolve_logs(product_xml)

            # Extract dependency tree from CBNG logs
            deps = CbngLogParser.parse_to_flat_dependencies(cbng_log)

            # Transform dependencies to JSON serializable form
            response_data = [dep.to_dict() for dep in deps]

            return jsonify(response_data)
        else:
            # Handle GET request for availability check
            return ""

    return app


if __name__ == '__main__':
    app = create_app()
    app.run('localhost', port='8080', debug=True)
