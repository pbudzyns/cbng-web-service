import gunicorn.app.base
from pathlib import Path

from cbng_web_service import create_app, config
from cbng_web_service.cbng_py import find_CBNG, get_CBNG


class CBNGApplication(gunicorn.app.base.Application):
    """
    WSGI gunicorn application for cbng-web-service app

    """

    def __init__(self, app, options):
        self.application = app
        self.options = options
        super().__init__()

    def load_config(self):
        config = {key: value for key, value in self.options.items()
                  if key in self.cfg.settings and value is not None}
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


if __name__ == '__main__':

    # Ensure CBNG is available otherwise downloads it
    try:
        find_CBNG()
    except RuntimeError:
        get_CBNG()

    # Create logging root path if not existing
    LOGGING_ROOT = Path(config.LOGGING_ROOT)
    if not LOGGING_ROOT.exists():
        LOGGING_ROOT.mkdir(parents=True, exist_ok=True)

    options = {
        'bind': f'{config.HOST}:{config.PORT}',
        'workers': config.NUM_WORKERS,
        'accesslog': f'{LOGGING_ROOT}/cbng_web_service_access.log',
        'errorlog': f'{LOGGING_ROOT}/cbng_web_service_error.log',
        'default_proc_name': 'cbng_web_service',
        'capture_output': True,
        'daemon': config.DAEMON
    }

    CBNGApplication(app=create_app(), options=options).run()
