import pytest

from cbng_web_service.dependencies import ARTIFACTORY_URL, Dependency, CbngLogParser


@pytest.fixture
def cbng_log():
    cbng_output = r"""
------------------------------------------------------------
Root project
------------------------------------------------------------

compile - Dependencies for source set 'main' (deprecated, use 'implementation' instead).
+--- <null>:accsoft-commons-domain:<null> -> cern.accsoft.commons:accsoft-commons-domain:2.9.0
|    \--- cern.accsoft.commons:accsoft-commons-util:4.3.2 -> 4.3.4
|         +--- com.google.code.findbugs:jsr305:3.0.2
|         \--- org.slf4j:slf4j-api:1.7.25
+--- <null>:japc:<null> -> cern.japc:japc:7.8.4
|    +--- cern.accsoft.commons:accsoft-commons-util:4.3.2 -> 4.3.4 (*)
|    +--- cern.cmw:cmw-directory-client:2.4.0
|    |    +--- cern.accsoft.commons:accsoft-commons-util:4.3.0 -> 4.3.4 (*)
|    |    +--- org.slf4j:slf4j-api:1.7.25
|    |    \--- cern.cmw:cmw-util:2.2.0
|    |         \--- org.slf4j:slf4j-api:1.7.25
|    +--- cern.japc:japc-value:7.8.4
|    |    \--- cern.accsoft.commons:accsoft-commons-util:4.3.2 -> 4.3.4 (*)
|    \--- org.slf4j:slf4j-api:1.7.25
\--- org.springframework:spring-context:5.2.0.RELEASE
"""
    return cbng_output


def test_dependency_uri():
    uri = Dependency("cern.accsoft.commons", "accsoft-commons-util", "4.3.4").uri
    assert uri == ARTIFACTORY_URL + "/cern/accsoft/commons/accsoft-commons-util/4.3.4/accsoft-commons-util-4.3.4.jar"


def test_jar_filename():
    jar_filename = Dependency(groupId="x.x", artifactId="some-dependency", version="0.1.2").jar_filename
    assert jar_filename == "some-dependency-0.1.2.jar"


def test_dependency_to_dict():
    dep = Dependency(groupId="cern.accsoft.commons", artifactId="accsoft-commons-util", version="4.3.4")
    assert dep.to_dict() == {
        "groupId": "cern.accsoft.commons",
        "artifactId": "accsoft-commons-util",
        "version": "4.3.4",
        "uri": ARTIFACTORY_URL + "/cern/accsoft/commons/accsoft-commons-util/4.3.4/accsoft-commons-util-4.3.4.jar",
        "jar_filename": "accsoft-commons-util-4.3.4.jar",
    }


def test_dependency_from_cbng_log_line():
    line = "org.springframework:spring-context:5.2.0.RELEASE"
    line2 = "cern.accsoft.commons:accsoft-commons-util:4.3.2 -> 4.3.4 (*)"
    assert Dependency.new_from_cbng_line(line) == Dependency("org.springframework", "spring-context", "5.2.0.RELEASE")
    assert Dependency.new_from_cbng_line(line2) == Dependency("cern.accsoft.commons", "accsoft-commons-util", "4.3.4")


def test_parse_cbng_to_flat_dependencies(cbng_log):

    deps_json = CbngLogParser.parse_to_flat_dependencies(cbng_log)

    assert deps_json == [
        Dependency("cern.accsoft.commons", "accsoft-commons-domain", "2.9.0"),
        Dependency("cern.accsoft.commons", "accsoft-commons-util", "4.3.4"),
        Dependency("com.google.code.findbugs", "jsr305", "3.0.2"),
        Dependency("org.slf4j", "slf4j-api", "1.7.25"),
        Dependency("cern.japc", "japc", "7.8.4"),
        Dependency("cern.cmw", "cmw-directory-client", "2.4.0"),
        Dependency("cern.cmw", "cmw-util", "2.2.0"),
        Dependency("cern.japc", "japc-value", "7.8.4"),
        Dependency("org.springframework", "spring-context", "5.2.0.RELEASE")
    ]

