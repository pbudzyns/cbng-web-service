import json
from flask import Response
import pytest
from unittest.mock import patch

from cbng_web_service import create_app
from cbng_web_service.cbng_py import get_CBNG
from cbng_web_service.dependencies import Dependency


@pytest.fixture
def app_client():
    app = create_app()
    app.config['TESTING'] = True

    with app.test_client() as client:
        yield client


@pytest.fixture
def require_cbng_instance():
    try:
        get_CBNG()
    except ValueError as e:
        print(e)
    finally:
        return


def test_resolve_product_xml_mocked(app_client):
    product_xml = """
            <products>
                <product directory="cmmnbuild_dep_manager" name="cmmnbuild_dep_manager" version="2.6.1.dev3+gf1664c5.d20200820">
                    <dependencies>
                    <dep product="log4j" /></dependencies>
                </product>
            </products>
        """

    fake_log = r"""
------------------------------------------------------------
Root project
------------------------------------------------------------

compile - Dependencies for source set 'main' (deprecated, use 'implementation' instead).
+--- <null>:accsoft-commons-domain:<null> -> cern.accsoft.commons:accsoft-commons-domain:2.9.0
|    \--- cern.accsoft.commons:accsoft-commons-util:4.3.2 -> 4.3.4
|         +--- com.google.code.findbugs:jsr305:3.0.2
|         \--- org.slf4j:slf4j-api:1.7.25
\--- org.springframework:spring-context:5.2.0.RELEASE
"""
    expected_dependencies = [
        Dependency("cern.accsoft.commons", "accsoft-commons-domain", "2.9.0"),
        Dependency("cern.accsoft.commons", "accsoft-commons-util", "4.3.4"),
        Dependency("com.google.code.findbugs", "jsr305", "3.0.2"),
        Dependency("org.slf4j", "slf4j-api", "1.7.25"),
        Dependency("org.springframework", "spring-context", "5.2.0.RELEASE")
    ]

    with patch('cbng_web_service.cbng_get_resolve_logs', lambda x: fake_log):
        response = app_client.post("/resolve/productxml", data=product_xml)

        assert json.loads(response.data) == [dep.to_dict() for dep in expected_dependencies]


def test_get_request(app_client):
    response = app_client.get("/resolve/productxml")
    assert response.status == Response(status=200).status


def test_install_cbng_and_resolve_product_xml(app_client, require_cbng_instance):

    product_xml = """
        <products>
            <product directory="cmmnbuild_dep_manager" name="cmmnbuild_dep_manager" version="2.6.1.dev3+gf1664c5.d20200820">
                <dependencies>
                <dep product="log4j" /></dependencies>
            </product>
        </products>
    """

    response = app_client.post("/resolve/productxml", data=product_xml)

    for dep in json.loads(response.data):
        assert "artifactId" in dep
        assert "groupId" in dep
        assert "version" in dep
        assert "uri" in dep