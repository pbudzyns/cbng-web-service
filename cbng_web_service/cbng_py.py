from pathlib import Path
import ssl
import subprocess
import socket
import tarfile
import tempfile
import urllib.request


PKG_ROOT = Path(__file__).absolute().parent
STD_CBNG = Path("/user/pcrops/devtools/CBNG/PRO/bin/bob")
VENDOR = PKG_ROOT / ".vendor"
LOCAL_CBNG = VENDOR / "commonbuild-ng-binaries" / "gradle" / "bin" / "bob"
DIRECT_CBNG_URL = "https://artifactory.cern.ch/release/cern/devops/cbng/3.8.3/cbng-3.8.3-bin.tgz"


def find_CBNG():
    if LOCAL_CBNG.exists():
        return LOCAL_CBNG
    elif STD_CBNG.exists():
        return STD_CBNG
    else:
        raise RuntimeError("Unable to find CBNG")


def get_CBNG():
    """
    Downloads CBNG from CERN's Artifactory instance.

    """
    if LOCAL_CBNG.exists():
        raise ValueError(f"Local CBNG already downloaded at {LOCAL_CBNG}")
    with tempfile.NamedTemporaryFile() as tmpfile:
        socket.setdefaulttimeout(10)  # Set timeout for downloading
        ssl._create_default_https_context = ssl._create_unverified_context
        urllib.request.urlretrieve(DIRECT_CBNG_URL, tmpfile.name)
        with tarfile.open(tmpfile.name, "r:gz") as tarball:
            tarball.extractall(VENDOR)


def cbng_get_resolve_logs(product_xml: str):
    with tempfile.TemporaryDirectory() as tmp_workdir:
        workdir = Path(tmp_workdir)
        with (workdir / 'product.xml').open('wt') as fh:
            fh.write(product_xml)

        cmd = [
            find_CBNG(),
            'dependencies',
            '--configuration=compile',
        ]

        cbng_logs = subprocess.check_output(cmd, cwd=workdir)
        return cbng_logs.decode('utf-8')
