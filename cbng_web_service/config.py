import os
import multiprocessing


def number_of_workers():
    return (multiprocessing.cpu_count() * 2) + 1


def check_env_variable_int(name, default):
    value = os.getenv(name, default)
    try:
        value = int(value)
        if value < 0:
            raise ValueError
    except ValueError:
        raise ValueError(f'Invalid {name} value {value}')
    finally:
        return value


_true_values = {'y', 'yes', '1', 'true'}
DAEMON = os.getenv("CBNG_WEB_SERVICE_RUN_DAEMON", "false").lower() in _true_values
LOGGING_ROOT = os.getenv("CBNG_WEB_SERVICE_LOGS_DIR", "/var/log/cbng-web-service").lower()
HOST = os.getenv("CBNG_WEB_SERVICE_HOST", "0.0.0.0")
PORT = check_env_variable_int("CBNG_WEB_SERVICE_PORT", "5000")
NUM_WORKERS = check_env_variable_int("CBNG_WEB_SERVICE_WORKERS", number_of_workers())
